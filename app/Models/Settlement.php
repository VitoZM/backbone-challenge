<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Settlement extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'settlement_id',
        'key',
        'name',
        'zone_type',
        'settlement_type',
        'zip_id',
    ];

    protected $primaryKey = 'settlement_id';
}
