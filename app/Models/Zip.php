<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Zip extends Model
{
    use HasFactory;

    protected $fillable = [
        'zip_id',
        'zip_code',
        'locality',
        'federal_entity',
        'settlements',
        'municipality',
        'json',
    ];

    protected $primaryKey = 'zip_id';
}
