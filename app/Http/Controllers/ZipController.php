<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Zip;
use App\Models\Settlement;

//falta arreglar el federal->key, poner a mayusculas todo, settlements->key y el settlement_type según el .txt

class ZipController extends Controller
{
    public function find(Request $request) {
        $zip_code = $request->zip_code;
        $zip = Zip::select('json')->where('zip_code',$zip_code)->first();
        if($zip == null)
            abort(404,'PAGE NOT FOUND');
        $zip = json_decode($zip->json);
        unset($zip->zip_id);
        unset($zip->updated_at);
        unset($zip->created_at);
        unset($zip->json);
        $zip->settlements = $zip->settlements;
        $zip->federal_entity = json_decode($zip->federal_entity);
        $zip->municipality = json_decode($zip->municipality);

        for ($i=0; $i<count($zip->settlements); $i++) {
            $zip->settlements[$i]->settlement_type = json_decode($zip->settlements[$i]->settlement_type);
            unset($zip->settlements[$i]->zip_id);
            unset($zip->settlements[$i]->updated_at);
            unset($zip->settlements[$i]->created_at);
            unset($zip->settlements[$i]->settlement_id);
        }
        
        return response()->json($zip,200);
    }

    public function findSettlement(Request $request) {
        $key = $request->key;
        $settlements = Settlement::join('zips','zips.zip_id','settlements.zip_id')
                                    ->select('settlements.*','zips.zip_code')
                                    ->where('key',$key)
                                    ->get();
        if($settlements == null || count($settlements) <= 0)
            abort(404,'PAGE NOT FOUND');
        for ($i=0; $i<count($settlements); $i++) {
            $settlements[$i]->settlement_type = json_decode($settlements[$i]->settlement_type);
            unset($settlements[$i]->zip_id);
        }
        return response()->json($settlements,200);
    }

    private static function formatLine($line) {
        $line = implode("A",explode("á",$line));
        $line = implode("E",explode("é",$line));
        $line = implode("I",explode("í",$line));
        $line = implode("O",explode("ó",$line));
        $line = implode("U",explode("ú",$line));
        $line = implode("A",explode("Á",$line));
        $line = implode("E",explode("É",$line));
        $line = implode("I",explode("Í",$line));
        $line = implode("O",explode("Ó",$line));
        $line = implode("U",explode("Ú",$line));
        $line = strtoupper($line);
        $lineParts = explode("|",$line);
        return $lineParts;
    }

    public function loadDatabase() {
        $txt_file = fopen(storage_path().'/CPdescarga.txt','r');
        $a = 0;
        $prev = "";
        $prevMunicipality = "";
        $zip = new Zip([]);
        $settlements = array();
        while ($line = utf8_encode( fgets($txt_file) )) {
            $a++;
            if ($a <= 2)
                continue;

            $settlement_type = explode("|",$line)[2];
            $lineParts = self::formatLine($line);

            $zip_code = $lineParts[0];
            $locality = $lineParts[5];
            $federal_entity = $lineParts[4];

            $key = $lineParts[7];
            $federal_entity = '{"key":'.intval($key).',"name":"'.$federal_entity.'","code":null}';
            $municipality = $lineParts[3];
            $key = $lineParts[count($lineParts)-4];
            $municipality = '{"key":'.intval($key).',"name":"'.$municipality.'"}';

            if ($zip_code != $prev) {
                if ($prev != "") {
                    $zip->settlements = $settlements;
                    $zip->municipality = $prevMunicipality;
                    $zip->json = json_encode($zip);
                    $zip->save();
                } 

                $zip = new Zip([
                    'zip_code' => $zip_code,
                    'locality' => $locality,
                    'federal_entity' => $federal_entity,
                ]);

                $settlements = array();
                $prevMunicipality = $municipality;
                $zip->save();
            }
            
            $key = $lineParts[count($lineParts)-3];
            $name = $lineParts[1];
            $zone_type = $lineParts[count($lineParts)-2];

            $settlement = new Settlement([
                'key' => intval($key),
                'name' => $name,
                'zone_type' => $zone_type,
                'settlement_type' => '{"name":"'.$settlement_type.'"}',
            ]);
            
            array_push($settlements,$settlement);

            $settlement->zip_id = $zip->zip_id;
            $settlement->save();
            
            $prev = $zip_code;
        }
        
        $zip->settlements = $settlements;
        $zip->municipality = $prevMunicipality;
        $zip->json = json_encode($zip);
        $zip->save();
        
        fclose($txt_file);
        return response()->json(["success" => true],200);
    }

}
