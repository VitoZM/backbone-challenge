<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettlementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settlements', function (Blueprint $table) {
            $table->increments('settlement_id');
            $table->integer('key');
            $table->string('name',100)->default("");
            $table->string('zone_type',100)->default("");
            $table->text('settlement_type');
            $table->timestamps();
            $table->unsignedInteger('zip_id');
            $table->foreign('zip_id')->references('zip_id')->on('zips');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settlements');
    }
}
