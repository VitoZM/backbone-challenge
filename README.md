<p align="center"><a href="https://jobs.backbonesystems.io/" target="_blank"><img src="https://backbone-challenge.tk/images/bkbn.png" width="400"></a></p>

<p align="center"></p>
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## About Me

<p>Hello I'm Alvaro from Bolivia, I've been a web developer for the past 4 years.</p>
<p>I know many programming languages but what I know most for the backend are php with laravel and JS with Node.JS now for the frontend Jquery with bootstrap and also React finally for databases I have too much expirience with SQL and MySQL, I consider my self as a back end developer but jobs made me learn Front end so i'm full stack right now.</p>
<p>The last works that i developed were 2 online stores, one of them is to buy clothes and the other one to purchase virtual credits for videogames I developed them with Laravel here you can see:</p>

- [GALAXY GAME STORE](https://galaxygame.store).
- [ZARGA701](https://zarga701.com).

You can see a little more of my portfolio [here](https://alvaroz.tk).

## About The Challenge

<p>First of all, I downloaded the entire data from [Correos de Mexico](https://www.correosdemexico.gob.mx/SSLServicios/ConsultaCP/CodigoPostal_Exportar.aspx) in .txt format, then I developed an algorithm to insert all the data into a MySQL database, then I debugged the data comparing to the example API, finally I uploaded the project to git and launched it in web.</p>
<p>In addition I developed an API to find settlements by its key.</p>
<p>Due to database is splited by zips and settlements It's easy to develop new APIs for querys, so the project is scalable.</p>

## APIS
- https://backbone-challenge.tk/api/zip-codes/{zip_code}
- https://backbone-challenge.tk/api/settlements/{key}
